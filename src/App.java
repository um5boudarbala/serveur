import org.graphstream.graph.Graph;
import org.graphstream.graph.Node;
import org.graphstream.graph.implementations.DefaultGraph;
import org.graphstream.graph.implementations.MultiGraph;
import org.graphstream.stream.binary.ByteProxy;
import org.graphstream.stream.netstream.NetStreamUtils;
import org.graphstream.util.VerboseSink;

import java.io.IOException;
import java.net.InetAddress;
public class App {
    public static void main(String[] args) throws IOException {
        System.setProperty("org.graphstream.ui", "swing");
        InetAddress ip = InetAddress.getLocalHost();
        System.out.println("Server IP address: " + ip.getHostAddress());
        
        System.out.println("server...");

        Graph g = new MultiGraph("G",false,true);

        VerboseSink logout = new VerboseSink();
        logout.setPrefix("server logout");
        g.addSink(logout);

        ByteProxy server = null;
        try {
            server = new ByteProxy(NetStreamUtils.getDefaultNetStreamFactory(), 8080);
        } catch (IOException e) {
            e.printStackTrace();
        }
        server.addSink(g);
        server.start();
        g.display();
    }
}
